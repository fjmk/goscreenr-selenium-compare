// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string
var importFile string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "goscreenr",
	Short: "A GO alternative for wraith-selenium, visually comparing two URL's.",
	Long: `GoSreener takes screenshots from one or more pages on 2 URL's with a
selenium service. With selenium you can use real browsers (Firefox, Chrome or IE) to take the screenshows.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	RootCmd.DisableFlagParsing = false

	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags, which, if defined here,
	// will be global for your application.

	//RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is ./config.yaml)")
	RootCmd.Flags().MarkHidden("flags")
	RootCmd.SetUsageTemplate("")
	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if len(os.Args) == 3 {
		cfgFile = os.Args[2]
	}
	if cfgFile != "" { // specify config file as first argument after command
		viper.SetConfigFile(cfgFile)
	} else {
		viper.SetConfigName("config") // name of config file (without extension)
		viper.AddConfigPath(".")      // adding current directory as first search path
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

	// import file from config file and merge given configfile
	if importFile := viper.GetString("import"); importFile != "" {
		viper.SetConfigFile(importFile)
		if err := viper.ReadInConfig(); err == nil {
			fmt.Println("Using import file:", viper.ConfigFileUsed())
		}
		fmt.Println("cfgFile 2: ", cfgFile)
		if cfgFile != "" { // enable ability to specify config file via flag
			viper.SetConfigFile(cfgFile)
		} else {
			viper.SetConfigName("config") // name of config file (without extension)
			viper.AddConfigPath(".")      // adding current directory as first search path
		}
		if err := viper.MergeInConfig(); err == nil {
			fmt.Println("Merged import file:", viper.ConfigFileUsed())
		}
	}
	viper.AutomaticEnv() // read in environment variables that match

	fmt.Println("browser:", viper.GetString("browser"))
	fmt.Println("paths:", viper.GetStringMapString("paths"))
	fmt.Println("paths:", viper.GetStringMapStringSlice("paths"))
	fmt.Println("screen_widths:", viper.GetStringSlice("screen_widths"))
	fmt.Println("Allkeys", viper.AllKeys())
	fmt.Println("Allsettings", viper.AllSettings())

}
