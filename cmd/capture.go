// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"strings"
	"text/template"
	"github.com/spf13/cobra"
	lib "gitlab.com/fjmk/goscreenr/screenrlib"
	"github.com/spf13/viper"
	"os"
	"time"
)

var tmpl = `project {{.Project}}
paths {{range $pathname, $paths := .Paths}}
pathname {{$pathname}}
 paths{{$paths}}
 sizes {{.Sizes}}
{{end}}
`

func capture(cmd *cobra.Command, args []string) {
	t, err := template.New("").Parse(tmpl)
	if err != nil {
		fmt.Printf("parse error: %v\n", err)
		return
	}

	fmt.Println("capture called: " + strings.Join(args, " "))

	project := lib.Project{
		viper.GetString("directory"),
		time.Now().Format("_2 Jan 2006 at 15:04:05"),
		viper.GetFloat64("threshold"),
		make(map[string]lib.Path,0),
		make(map[string]string,0),
	}

	t.Execute(os.Stdout, project)

	if err := lib.CreateFolders(); err != nil {
		fmt.Printf("ERROR: Could not create %s/<paths> directories\n", viper.GetString("directory"))
		os.Exit(1)
	}

	if err := lib.ScreenShots(); err != nil {
		fmt.Println("Warning: Could not take all screenshots")
	}

	if err := lib.Compare(&project); err != nil {
		fmt.Println("Warning: Could not compare all screenshots")
	}
	t.Execute(os.Stdout, project)
	if err := lib.MakeGallery(&project, "gallery.html"); err != nil {
	  	fmt.Println("Warning: Could not create gallery.html")
	}
}

// captureCmd represents the capture command
var captureCmd = &cobra.Command{
	Use:   "capture",
	Short: "Capture and compare screenshots",
	Long: `Capture paths against two domains, compare them, generate gallery
without [config_name] it will use ./config.yaml`,
	Run: capture,
//	func(cmd *cobra.Command, args []string) {
		// TODO: Work your own magic here
//		fmt.Println("capture called: " + strings.Join(args, " "))
//	},
}

func init() {
	RootCmd.AddCommand(captureCmd)
	captureCmd.SetUsageTemplate(`Usage:
goscreenr capture [config_name]
`)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// captureCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// captureCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}
