// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"gitlab.com/fjmk/goscreenr/cmd"
	"github.com/spf13/viper"
)

func init() {
	viper.SetDefault("directory", ".")
	viper.SetDefault("browser", "firefox")
	viper.SetDefault("waittimeout", 60)
	viper.SetDefault("scripttimeout", 60)
	viper.SetDefault("pageloadtimeout", 15)
}

func main() {
	cmd.Execute()
}
