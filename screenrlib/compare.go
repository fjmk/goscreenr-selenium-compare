package screenrlib

import (
	"fmt"
	"github.com/spf13/viper"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

// Compare screenshots.
func Compare(project *Project) error {
	var size []string
	var domainUrls []string
	var domainNames []string
	var diffPercentage float64
	//	var fileNames []string

	viper.SetDefault("highlight_color", "mediumvioletred")

	// Get variables from config
	browser := viper.GetString("browser")
	paths := viper.GetStringMapString("paths")
	domains := viper.GetStringMapString("domains")
	for domainName, domainUrl := range domains {
		domainNames = append(domainNames, domainName)
		domainUrls = append(domainUrls, domainUrl)
	}
	fmt.Println(domains)
	project.Domains = domains
	fmt.Println("domainUrls: ", domainUrls)
	fmt.Println("domainNames: ", domainNames)
	screenWidths := viper.GetStringSlice("screen_widths")

	for folder, path := range paths {
		sizes := make(map[int64]Size)
		maxPercentage := 0.00
		for _, screenWidth := range screenWidths {
			fileNames := make(map[string]string, 0)
			fileNames[domainNames[0]] = Filename(screenWidth, browser, domainNames[0]) + ".png"
			fileNames[domainNames[1]] = Filename(screenWidth, browser, domainNames[1]) + ".png"
			diffFilename := Filename(screenWidth, browser, "diff.png")
			if size = strings.Split(screenWidth, "x"); len(size) == 1 {
				// fmt.Println(size)
				size = append(size, "1500")
			}
			//width, err := strconv.Atoi(size[0])
			//height, err := strconv.Atoi(size[1])
			options := []string{
				"-dissimilarity-threshold 1",
				"-fuzz " + viper.GetString("fuzz"),
				"-metric AE",
				"-highlight-color " + viper.GetString("highlight_color"),
				viper.GetString("directory") + "/" + folder + "/" + fileNames[domainNames[0]],
				viper.GetString("directory") + "/" + folder + "/" + fileNames[domainNames[1]],
				viper.GetString("directory") + "/" + folder + "/" + diffFilename,
			}
			parts := strings.Fields(strings.Join(options, " "))
			out, _ := exec.Command("compare", parts[0:]...).CombinedOutput()
			// err of compare always 1 when images are not the same
			if error := strings.Contains(string(out[:]), "error/"); error == true {
				fmt.Println("Something wrong with compare iamges: ", string(out[:])) // os.Exit(1)
				diffPercentage = 99999.00
			} else {
				diff, _ := strconv.ParseFloat(string(out[:]), 32)
				xSize, _ := strconv.ParseFloat(size[0], 32)
				ySize, _ := strconv.ParseFloat(size[1], 32)

				diffPercentage = float64(diff / (xSize * ySize) * 100)
				//fmt.Printf("Sixe: %sx%s   \tDifference: %.2f\n", size[0], size[1], diffPercentage)
				// write difference to fileName[0].txt
				//fileName := viper.GetString("directory") + "/" + folder + "/" + Filename(screenWidth, browser, "diff") + ".txt"
				// werkt niet fmt.Println([]byte(diffPercentage))
				err := ioutil.WriteFile(viper.GetString("directory")+"/"+folder+"/"+Filename(screenWidth, browser, "diff")+".txt", []byte(fmt.Sprintf(".2%f", diffPercentage)), 0664)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
			}
			// fill render array
			sizeInt, _ := strconv.ParseInt(size[0], 10, 32)
			sizes[sizeInt] = Size{
				diffPercentage,
				fileNames,
				diffFilename,
			}
			if diffPercentage > maxPercentage {
				maxPercentage = diffPercentage
			}
		}
		project.Paths[folder] = Path{
			path,
			sizes,
			maxPercentage,
		}

	}

	return nil
}
