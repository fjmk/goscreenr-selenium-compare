package screenrlib

import (
	"fmt"
	"github.com/pkg/errors"
	"os"
	"os/exec"
	"strings"
	"text/template"
)

var tmpl = `<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <style type="text/css">
        .short-screenshot {
            max-width: 200px;
        }
        .panel li{
            word-wrap: break-word;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row page-header">
        <h1>List of screenshots for {{.Directory}} <small>taken {{.Time}}</small></h1>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <div class="panel">
                <div class="panel-heading">Screenshots:</div>
                <ul class="list-group list-group-flush">
                    	{{range $name, $path := .Paths}}
                    <li class="list-group-item"><a href="#{{$name}}">{{$name}}</a></li>
                    {{end}}
                </ul>
            </div>
        </div>
	<div class="col-lg-10">
            {{$treshold  := .Treshold}}
            {{$domains := .Domains}}
            {{range $name, $path := .Paths}}
            	{{range $size, $sizes := .Sizes}}
                <div class="row">
                <a name="{{$name}}"></a>
                {{if gt $sizes.Percentage $treshold}}
                <h2 style="color:#CC0101">{{$name}}> <span class="glyphicon glyphicon-remove"></span></h2>
                {{else}}
                <h2>{{$name}}</h2>
                {{end}}
            </div>

            <div class="row">
                <h3 class="col-lg-1 text-muted">{{$size}}px</h3>
                    {{range $domainname, $domainurl := $domains}}
                       {{range $dname, $fname := $sizes.Files}}
                         {{if eq $dname $domainname}} {{$filename := $fname}}
                <div class="col-lg-3">
                    <a href="{{$name}}/{{$filename}}">
                        <img class="short-screenshot img-thumbnail" src="{{$name}}/thumbnails/{{$filename}}">
                    </a>
                    <p class="text-center">
                        <a href="{{$domainurl}}{{$path.Uri}}" target="_blank">{{$domainname}}</a>
                    </p>
                </div>
                {{end}}{{end}}{{end}}
                <div class="col-lg-3">
                    {{if .DiffFilename}}
                    <a href="{{$name}}/{{$sizes.DiffFilename}}">
                        <img class="short-screenshot img-thumbnail" src="{{$name}}/thumbnails/{{$sizes.DiffFilename}}">
                    </a>
                    {{end}}
                    <p class="text-center">diff</p>
                    {{if gt $sizes.Percentage $treshold}}
                    <p style="color:#CC0101" class="text-center text-muted">{{$sizes.Percentage}} % different</p>
                    {{else}}
                    <p class="text-center text-muted">{{$sizes.Percentage}} % different</p>
                    {{end}}
                </div>
            </div>
             {{end}} {{end}}
        </div>
    </div>
</div>
</body>
</html>
`

func createTumbnail(directory string, folder string, file string) error {
	// convert #{png_path.shellescape} -thumbnail 200 -crop #{wraith.thumb_width.to_s}x#{wraith.thumb_height.to_s}+0+0 #{output_path.shellescape}
	options := []string{
		strings.Join([]string{directory, folder, file}, "/"),
		"-thumbnail 200",
		"-crop 1000x1000+0+0", // replace with gallery.thumb_width and gallery.thumb_height
		strings.Join([]string{directory, folder, "thumbnails", file}, "/"),
	}
	parts := strings.Fields(strings.Join(options, " "))
	out, _ := exec.Command("convert", parts[0:]...).CombinedOutput()
	// err of compare always 1 when images are not the same
	if error := strings.Contains(string(out[:]), "error/"); error == true {
		// fmt.Println("Something wrong with creating thumbnails: ", string(out[:]))  // os.Exit(1)
		return errors.New("Something wrong with creating thumbnails: " + string(out[:]))
	}
	return nil
}

func MakeGallery(project *Project, filename string) error {
	out, err := os.Create(project.Directory + "/" + filename)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println("Creating thumbnails...")
	for folder, path := range project.Paths {
		for _, files := range path.Sizes {
			for _, file := range files.Files {
				createTumbnail(project.Directory, folder, file)
			}
			createTumbnail(project.Directory, folder, files.DiffFilename)
		}
	}

	// Get template
	t, err := template.New("").Parse(tmpl)
	println("Create gallery.html...")

	// Create gallery.html
	err = t.Execute(out, project)
	if err != nil {
		fmt.Printf("exec error: %v\n", err)
		os.Exit(1)
	}
	return nil
}
