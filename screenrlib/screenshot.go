package screenrlib

import (
	"bytes"
	"fmt"
	"image"
	"image/png"
	"os"
	"strconv"
	"time"

	"strings"

	"github.com/spf13/viper"
	"github.com/tebeka/selenium"
)

func screenshot(wd selenium.WebDriver, name string) {
	screenshot, err := wd.Screenshot()
	if err != nil {
		fmt.Println("Something wrong with screenshot? ", err)
		return
	}
	img, _, _ := image.Decode(bytes.NewReader(screenshot))

	out, err := os.Create(name)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = png.Encode(out, img)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

}

// ScreenShots save all screenshots.
func ScreenShots() error {
	var (
		size []string
	)

	// Get variables from config
	browser := viper.GetString("browser")
	paths := viper.GetStringMapString("paths")
	domains := viper.GetStringMapString("domains")
	screenWidths := viper.GetStringSlice("screen_widths")
	//	waittimeout := viper.GetInt("waittimeout")
	//	scripttimeout := viper.GetInt("scripttimeout")
	pageloadtimeout := viper.GetInt("pageloadtimeout")

	// *** Add gecko driver here if necessary ***
	caps := selenium.Capabilities{
		"browserName": browser,
	}
	wd, err := selenium.NewRemote(caps, "http://localhost:4444/wd/hub")
	if err != nil {
		panic(err)
	}

	defer wd.Quit()
	if err := wd.SetPageLoadTimeout(time.Duration(pageloadtimeout) * time.Second); err != nil {
		fmt.Println("Error loading Page: ", err)
	}

	for folder, uri := range paths {
		for domainName, domainUrl := range domains {
			if err := wd.Get(domainUrl + uri); err != nil {
				fmt.Println("Error loading page: ", err)
				continue
			}
			wh, err := wd.CurrentWindowHandle()
			if err != nil {
				fmt.Println("windowhandle ", err)
			}

			fmt.Println(uri)
			fmt.Println(domainUrl)
			for _, screenWidth := range screenWidths {
				if size = strings.Split(screenWidth, "x"); len(size) == 1 {
					// fmt.Println(size)
					size = append(size, "1500")
				}
				width, _ := strconv.Atoi(size[0])
				height, _ := strconv.Atoi(size[1])
				filename := Filename(screenWidth, browser, domainName)
				fmt.Println(size)
				//wd.Get(domain + path)
				//	wd.Get("https://www.google.nl/")
				//				wd.SetImplicitWaitTimeout(time.Duration(waittimeout) * time.Second)
				//				wd.SetAsyncScriptTimeout(time.Duration(scripttimeout) * time.Second)
				if err := wd.ResizeWindow(wh, width, height); err != nil {
					fmt.Println("Error resizing page: ", err)
					continue
				}
				time.Sleep(time.Duration(1) * time.Second)
				//	page1Buttons, _ := wd.FindElements(selenium.ByXPATH, "//div[@type='button']")
				screenshot(wd, viper.GetString("directory")+"/"+folder+"/"+filename+".png")
			}
		}
	}

	return nil
}
