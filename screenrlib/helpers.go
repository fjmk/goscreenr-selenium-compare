package screenrlib

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
)

// Filename returns a clean filename
func Filename(screenWidths string, browser string, Name string) string {
	return screenWidths + "_" + browser + "_" + Name
}

// CreateFolders for all url-tags in screenshot directory
func CreateFolders() error {
	directory := viper.GetString("directory")
	fmt.Println(directory)
	// remove exiting files
	if err := os.RemoveAll(directory); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// create all paths
	allPaths := viper.GetStringMapString("paths")
	for thisPath := range allPaths {
		if err := os.MkdirAll(directory+"/"+thisPath+"/thumbnails", 0775); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
	fmt.Println("Folders created")
	return nil
}

type Project struct {
	Directory string
	Time      string
	Treshold  float64
	Paths     map[string]Path
	Domains   map[string]string
}

type Path struct {
	Uri           string
	Sizes         map[int64]Size
	maxPercentage float64
}

type Size struct {
	Percentage   float64
	Files        map[string]string
	DiffFilename string
}
